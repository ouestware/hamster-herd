import * as EmailValidator from 'email-validator';

export function checkEmail(email: string): boolean {
  return EmailValidator.validate(email);
}
