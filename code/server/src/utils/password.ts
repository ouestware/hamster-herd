import { passwordStrength, defaultOptions, DiversityType } from 'check-password-strength';

interface PasswordRule {
  name: string;
  description: string;
}

export const PASSWORD_RULES: Array<PasswordRule> = [
  { name: 'length', description: 'Must contain at least 8 characters' },
  { name: 'lowercase', description: 'Must contain at least 1 lowercase character' },
  { name: 'uppercase', description: 'Must contain at least 1 uppercase character' },
  { name: 'number', description: 'Must contain at least 1 numeric character' },
  { name: 'symbol', description: 'Must contain at least 1 special character (!@#$%^&*)' },
];

interface CheckPasswordResult {
  isValid: boolean;
  strength: string; // ie Too weak, Weak, Medium, Strong
  missingRules: Array<PasswordRule>; // lowercase, uppercase, symbol, number, length
}

/**
 * Check the password strength and says if it is a valid password or not.
 */
export function checkPassword(password: string): CheckPasswordResult {
  const strength = passwordStrength(password, defaultOptions);
  return {
    isValid: strength.id >= 2,
    strength: strength.value,
    missingRules: PASSWORD_RULES.filter((rule: PasswordRule) => {
      if (rule.name === 'length') return strength.length < 8;
      else return !strength.contains.includes(rule.name as DiversityType);
    }),
  };
}
