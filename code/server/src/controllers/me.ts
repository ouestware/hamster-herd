import { ApiBearerAuth, ApiOkResponse, ApiQuery, ApiTags } from '@nestjs/swagger';
import {
  Body,
  Controller,
  Delete,
  ForbiddenException,
  Get,
  NotFoundException,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  Request,
} from '@nestjs/common';

import { getLogger } from '../services/logger';
import { ActivityService } from '../services/activity';
import { UserDto } from '../models/user';
import {
  ActivityCreationDto,
  ActivityCursorResult,
  ActivityDto,
  ActivityUpdateDto,
  toActivityDto,
} from '../models/activity';
import { ActivityStatus } from '@prisma/client';
import { head } from 'lodash';

@ApiTags('me')
@ApiBearerAuth()
@Controller('me')
export class MeController {
  /**
   * Logger
   */
  private log = getLogger('MeController');

  /**
   * Default constructor
   */
  constructor(private activity: ActivityService) {
    this.log.info('initialized');
  }

  @Get('activity/current')
  @ApiOkResponse({ description: 'Get user current activity', type: ActivityDto })
  async currentActivity(@Request() req: { user: UserDto }): Promise<ActivityDto | null> {
    this.log.info('Current activity', { userId: req.user.id });
    const activities = await this.activity.search({ status: 'IN_PROGRESS', userId: req.user.id, limit: 1 });
    const activity = head(activities.result);
    return activity || null;
  }

  @Get('activity/search')
  @ApiQuery({ name: 'limit', required: false, type: Number })
  @ApiQuery({ name: 'cursor', required: false, type: Number })
  @ApiQuery({ name: 'startedAt', required: false, type: Date })
  @ApiQuery({ name: 'endedAt', required: false, type: Date })
  @ApiQuery({ name: 'status', required: false, enum: ActivityStatus })
  @ApiOkResponse({ description: 'Search in my activities', type: ActivityCursorResult })
  async searchActivities(
    @Request() req: { user: UserDto },
    @Query('startedAt') startedAt?: Date,
    @Query('endedAt') endedAt?: Date,
    @Query('status') status?: ActivityStatus,
    @Query('limit', new ParseIntPipe({ optional: true })) limit?: number,
    @Query('cursor', new ParseIntPipe({ optional: true })) cursor?: number,
  ): Promise<ActivityCursorResult> {
    this.log.info('Search activities', { userId: req.user.id });
    const activities = await this.activity.search({ startedAt, endedAt, status, userId: req.user.id, limit, cursor });
    return {
      ...activities,
      result: activities.result.map((a) => toActivityDto(a)),
    };
  }

  @Post('activity')
  @ApiOkResponse({ description: 'Create an activity', type: ActivityDto })
  async createActivity(@Request() req: { user: UserDto }, @Body() data: ActivityCreationDto): Promise<ActivityDto> {
    this.log.info('Create an activity', { userId: req.user.id, data });
    const activity = await this.activity.create({ description: null, tags: [], ...data, userId: req.user.id });
    return toActivityDto(activity);
  }

  @Put('activity')
  @ApiOkResponse({ description: 'Update an activity', type: ActivityDto })
  async updateActivity(
    @Request() req: { user: UserDto },
    @Param('id', ParseIntPipe) id: number,
    @Body() data: ActivityUpdateDto,
  ): Promise<ActivityDto> {
    this.log.info('Update an activity', { userId: req.user.id, id, data });

    // Some checks
    const activity = await this.activity.findById(id);
    if (!activity) throw new NotFoundException();
    if (activity.userId !== req.user.id) throw new ForbiddenException();

    // Do update
    const activityUpdated = await this.activity.update(id, data);
    if (!activityUpdated) throw new NotFoundException();

    return toActivityDto(activityUpdated);
  }

  @Delete('activity/:id')
  @ApiOkResponse({ description: 'Delete an activity', type: ActivityDto })
  async deleteActivity(@Request() req: { user: UserDto }, @Param('id', ParseIntPipe) id: number): Promise<ActivityDto> {
    this.log.info('Delete an activity', { userId: req.user.id, id });

    // Some checks
    const activity = await this.activity.findById(id);
    if (!activity) throw new NotFoundException();
    if (activity.userId !== req.user.id) throw new ForbiddenException();

    // Deletion
    await this.activity.delete(id);

    return toActivityDto(activity);
  }
}
