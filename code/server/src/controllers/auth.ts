import { ApiBearerAuth, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { Body, Controller, Get, Post, Request, UnauthorizedException } from '@nestjs/common';

import { UserDto, UserLoginDto, toUserDto } from '../models/user';
import { Public } from '../auth/public.decorator';
import { getLogger } from '../services/logger';
import { UserService } from '../services/user';
import { AuthService } from '../services/auth';

@ApiTags('auth')
@ApiBearerAuth()
@Controller('auth')
export class AuthController {
  /**
   * Logger
   */
  private log = getLogger('AuthController');

  /**
   * Default constructor
   */
  constructor(
    private user: UserService,
    private auth: AuthService,
  ) {
    this.log.info('initialized');
  }

  @Post('login')
  @Public()
  @ApiOkResponse({ description: 'Sign-in', type: String })
  async login(@Body() data: UserLoginDto): Promise<string> {
    this.log.info('Login', data.email);
    const jwt = await this.auth.login(data.email, data.password);
    if (!jwt) throw new UnauthorizedException();
    return jwt;
  }

  @Get('whoami')
  @ApiOkResponse({ description: 'Get users', type: UserDto })
  async whoami(@Request() req: { user: UserDto }): Promise<UserDto> {
    this.log.info('Whomai');
    const user = await this.user.findById(req.user.id);
    if (!user) throw new UnauthorizedException();
    return toUserDto(user);
  }
}
