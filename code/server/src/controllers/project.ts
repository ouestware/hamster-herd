import { ApiBearerAuth, ApiOkResponse, ApiQuery, ApiTags } from '@nestjs/swagger';
import {
  Body,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
} from '@nestjs/common';

import { ProjectCreationDto, ProjectCursorResult, ProjectDto, ProjectUpdateDto, toProjectDto } from '../models/project';
import { getLogger } from '../services/logger';
import { ProjectService } from '../services/project';
import { TimeBudgetService } from '../services/timeBudget';
import {
  TimeBudgetDto,
  TimeBudgetUpdateDto,
  TimeBudgetCreationDto,
  TimeBudgetCursorResult,
  toTimeBudgetDto,
} from '../models/timeBudget';

@ApiBearerAuth()
@ApiTags('project')
@Controller('project')
export class ProjectController {
  /**
   * Logger
   */
  private log = getLogger('UserController');

  /**
   * Default constructor
   */
  constructor(
    private project: ProjectService,
    private budget: TimeBudgetService,
  ) {
    this.log.info('initialized');
  }

  //
  // Project routes
  //

  @Get()
  @ApiQuery({ name: 'name', required: false, type: String })
  @ApiQuery({ name: 'tags', required: false, isArray: true, type: String })
  @ApiQuery({ name: 'limit', required: false, type: Number })
  @ApiQuery({ name: 'cursor', required: false, type: Number })
  @ApiOkResponse({ description: 'Search projects', type: ProjectDto })
  async search(
    @Query('name') name?: string,
    @Query('tags') tags?: string[],
    @Query('limit', new ParseIntPipe({ optional: true })) limit?: number,
    @Query('cursor', new ParseIntPipe({ optional: true })) cursor?: number,
  ): Promise<ProjectCursorResult> {
    this.log.info(`Search`);
    return this.project.search({ name, tags, limit, cursor });
  }

  @Get(':projectId')
  @ApiOkResponse({ description: 'Retrieve a project', type: ProjectDto })
  async get(@Param('projectId', ParseIntPipe) id: number): Promise<ProjectDto> {
    this.log.info(`Get project`, id);
    const project = await this.project.findById(id);
    if (!project) throw new NotFoundException();
    return toProjectDto(project);
  }

  @Post()
  @ApiOkResponse({ description: 'Create a new project', type: ProjectDto })
  async create(@Body() data: ProjectCreationDto): Promise<ProjectDto> {
    this.log.info(`Create project`);
    const project = await this.project.create({ description: null, tags: [], ...data });
    return toProjectDto(project);
  }

  @Put(':projectId')
  @ApiOkResponse({ description: 'Update a project', type: ProjectDto })
  async update(@Param('projectId', ParseIntPipe) id: number, @Body() data: ProjectUpdateDto): Promise<ProjectDto> {
    this.log.info(`Update project`, id);
    const project = await this.project.update(id, data);
    if (!project) throw new NotFoundException();
    return toProjectDto(project);
  }

  @Delete(':projectId')
  @ApiOkResponse({ description: 'Update a project', type: ProjectDto })
  async delete(@Param('projectId', ParseIntPipe) id: number): Promise<ProjectDto> {
    this.log.info(`Delete project`, id);
    const project = await this.project.delete(id);
    if (!project) throw new NotFoundException();
    return toProjectDto(project);
  }

  //
  // Budget routes
  //

  @Get(':projectId/budget')
  @ApiQuery({ name: 'limit', required: false, type: Number })
  @ApiQuery({ name: 'cursor', required: false, type: Number })
  @ApiOkResponse({ description: "Search in project's budgets", type: TimeBudgetCursorResult })
  async budgetSearch(
    @Param('projectId', ParseIntPipe) projectId: number,
    @Query('limit', new ParseIntPipe({ optional: true })) limit?: number,
    @Query('cursor', new ParseIntPipe({ optional: true })) cursor?: number,
  ): Promise<TimeBudgetCursorResult> {
    this.log.info(`Search project's budget`, { projectId, limit, cursor });
    const budgets = await this.budget.search({ projectId, limit, cursor });
    return {
      ...budgets,
      result: budgets.result.map((b) => toTimeBudgetDto(b)),
    };
  }

  @Get(':projectId/budget/:budgetId')
  @ApiOkResponse({ description: "Retrieve a project's budget", type: TimeBudgetDto })
  async budgetGet(
    @Param('projectId', ParseIntPipe) projectId: number,
    @Param('budgetId', ParseIntPipe) budgetId: number,
  ): Promise<TimeBudgetDto> {
    const budget = await this.budget.findById(budgetId);
    if (!budget || budget?.projectId !== projectId) throw new NotFoundException();
    return toTimeBudgetDto(budget);
  }

  @Post(':projectId/budget')
  @ApiOkResponse({ description: "Create a new project's budget", type: TimeBudgetDto })
  async budgetCreation(
    @Param('projectId', ParseIntPipe) projectId: number,
    @Body() data: TimeBudgetCreationDto,
  ): Promise<TimeBudgetDto> {
    this.log.info(`Create project's budget`, { projectId, data });
    const budget = await this.budget.create(projectId, { description: null, ...data });
    if (!budget) throw new NotFoundException();
    return toTimeBudgetDto(budget);
  }

  @Put(':projectId/budget/:budgetId')
  @ApiOkResponse({ description: 'Update a project', type: TimeBudgetDto })
  async budgetUpdate(
    @Param('projectId', ParseIntPipe) projectId: number,
    @Param('budgetId', ParseIntPipe) budgetId: number,
    @Body() data: TimeBudgetUpdateDto,
  ): Promise<TimeBudgetDto> {
    this.log.info(`Update project's budget`, { projectId, budgetId, data });

    // Check
    const budget = await this.budget.findById(budgetId);
    if (!budget || budget?.projectId !== projectId) throw new NotFoundException();

    // Do update
    const updatedBudget = await this.budget.update(budgetId, data);
    if (!updatedBudget) throw new NotFoundException();

    // Return
    return toTimeBudgetDto(updatedBudget);
  }

  @Delete(':projectId/budget/:budgetId')
  @ApiOkResponse({ description: 'Update a project', type: TimeBudgetDto })
  async budgetDelete(
    @Param('projectId', ParseIntPipe) projectId: number,
    @Param('budgetId', ParseIntPipe) budgetId: number,
  ): Promise<TimeBudgetDto> {
    this.log.info(`Delete project's budget`, { projectId, budgetId });

    // Check
    const budget = await this.budget.findById(budgetId);
    if (!budget || budget?.projectId !== projectId) throw new NotFoundException();

    // Do delete
    const deletedBudget = await this.budget.delete(budgetId);
    if (!deletedBudget) throw new NotFoundException();

    // Return
    return toTimeBudgetDto(deletedBudget);
  }
}
