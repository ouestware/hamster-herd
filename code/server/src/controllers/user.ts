import { ApiBearerAuth, ApiOkResponse, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Body, Controller, Get, NotFoundException, Param, ParseIntPipe, Post, Put, Query } from '@nestjs/common';

import { UserCursorResult } from '../models/user';
import { UserDto, UserCreationDto, UserUpdateDto, toUserDto } from '../models/user';
import { getLogger } from '../services/logger';
import { UserService } from '../services/user';

@ApiTags('user')
@ApiBearerAuth()
@Controller('user')
export class UserController {
  /**
   * Logger
   */
  private log = getLogger('UserController');

  /**
   * Default constructor
   */
  constructor(private user: UserService) {
    this.log.info('initialized');
  }

  @Get()
  @ApiQuery({ name: 'email', required: false, type: String })
  @ApiQuery({ name: 'name', required: false, type: String })
  @ApiQuery({ name: 'limit', required: false, type: Number })
  @ApiQuery({ name: 'cursor', required: false, type: Number })
  @ApiOkResponse({ description: 'Search users', type: UserCursorResult })
  async search(
    @Query('email') email?: string,
    @Query('name') name?: string,
    @Query('limit', new ParseIntPipe({ optional: true })) limit?: number,
    @Query('cursor', new ParseIntPipe({ optional: true })) cursor?: number,
  ): Promise<UserCursorResult> {
    this.log.info('Search users');
    const users = await this.user.search({ name, email, limit, cursor });
    return {
      ...users,
      result: users.result.map((u) => toUserDto(u)),
    };
  }

  @Get(':id')
  @ApiOkResponse({ description: 'Get users', type: UserDto })
  async get(@Param('id', ParseIntPipe) id: number): Promise<UserDto> {
    this.log.info('Get user', id);
    const user = await this.user.findById(id);
    if (!user) throw new NotFoundException();
    return toUserDto(user);
  }

  @Post()
  @ApiOkResponse({ description: 'Create a user', type: UserDto })
  async create(@Body() data: UserCreationDto): Promise<UserDto> {
    this.log.info('Create user');
    const user = await this.user.create(data);
    return toUserDto(user);
  }

  @Put(':id')
  @ApiOkResponse({ description: 'Update a user', type: UserDto })
  async update(@Param('id', ParseIntPipe) id: number, @Body() data: UserUpdateDto): Promise<UserDto> {
    this.log.info('Update user', id);
    const user = await this.user.update(id, data);
    if (!user) throw new NotFoundException();
    return toUserDto(user);
  }
}
