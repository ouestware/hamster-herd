import { APP_FILTER, APP_GUARD } from '@nestjs/core';
import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';

import { config } from './config';
// Controllers
import { AuthController } from './controllers/auth';
import { MeController } from './controllers/me';
import { ProjectController } from './controllers/project';
import { UserController } from './controllers/user';
// Services
import { ActivityService } from './services/activity';
import { AuthService } from './services/auth';
import { MailerService } from './services/mailer';
import { PrismaService } from './services/prisma';
import { ProjectService } from './services/project';
import { TimeBudgetService } from './services/timeBudget';
import { TokenService } from './services/token';
import { UserService } from './services/user';
// Misc
import { HttpExceptionFilter } from './filters/error';
import { AuthGuard } from './auth/auth.guard';

@Module({
  imports: [
    JwtModule.register({
      global: true,
      secret: config.secret,
      signOptions: { expiresIn: '5d' },
    }),
  ],
  controllers: [AuthController, MeController, ProjectController, UserController],
  providers: [
    {
      provide: APP_FILTER,
      useClass: HttpExceptionFilter,
    },
    {
      provide: APP_GUARD,
      useClass: AuthGuard,
    },
    ActivityService,
    AuthService,
    MailerService,
    ProjectService,
    PrismaService,
    TimeBudgetService,
    TokenService,
    UserService,
  ],
})
export class AppModule {}
