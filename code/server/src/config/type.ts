import SMTPTransport from 'nodemailer/lib/smtp-transport';
import JSONTransport from 'nodemailer/lib/json-transport';

export type LogLevel = 'error' | 'warning' | 'info' | 'debug' | 'off';
interface ConfigLog {
  filter: string;
  console_level: LogLevel;
  file_level: LogLevel;
  file_maxsize: string;
  file_retention: string;
  file_path: string;
}

interface ConfigMailer {
  noReply: string;
  transport: SMTPTransport.Options | JSONTransport.Options;
}

export interface Config {
  // Host / IP on which the server listen
  host: string;
  // Port on which the server is listen
  port: number;
  // Public url of the application
  baseUrl: string;
  // Secret
  secret: string;
  // Admin user
  initAdmin: { email: string; password: string };
  // Logs configuration
  logs: ConfigLog;
  // Mail configuration
  mailer: ConfigMailer;
}
