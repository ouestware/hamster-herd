import { Config, LogLevel } from './type';

export const config: Config = {
  host: process.env.BACKEND_HOST ? process.env.BACKEND_HOST : 'localhost',
  port: process.env.BACKEND_PORT ? Number(process.env.BACKEND_PORT) : 4000,
  baseUrl: process.env.BASE_URL || 'http://localhost',
  secret: process.env.BACKEND_SECRET || 'AZERTYUIOP',
  initAdmin: {
    email: process.env.BACKEND_ADMIN_EMAIL || 'contact@ouestware.com',
    password: process.env.BACKEND_ADMIN_PASSWORD || '_0ct4V3_',
  },
  logs: {
    filter: process.env.LOG_FILTER || '.*',
    console_level: (process.env.LOG_CONSOLE_LEVEL || 'debug') as LogLevel,
    file_level: (process.env.LOG_FILE_LEVEL || 'off') as LogLevel,
    file_maxsize: '200m',
    file_retention: '7d',
    file_path: './',
  },
  mailer: {
    noReply: 'Octave<no-reply@ouestware.com>',
    transport: process.env.SMTP_HOST
      ? {
          host: process.env.SMTP_HOST,
          port: process.env.SMTP_PORT ? Number(process.env.SMTP_PORT) : 25,
          auth: {
            user: process.env.SMTP_USER || '',
            pass: process.env.SMTP_PASSWORD || '',
          },
        }
      : {
          jsonTransport: true,
        },
  },
};
