import { Injectable } from '@nestjs/common';

import { TimeBudget } from '@prisma/client';
import { OctaveBusinessError } from '../models/error';
import { CursorResult } from '../models';
import { getLogger } from './logger';
import { PrismaService } from './prisma';
import { ProjectService } from './project';
import { last } from 'lodash';

type TimeBudgetCreation = Omit<TimeBudget, 'id' | 'createdAt' | 'updatedAt' | 'projectId'>;

@Injectable()
export class TimeBudgetService {
  /**
   * Logger.
   */
  private log = getLogger('TimeBudgetService');

  /**
   * Default constructor.
   */
  constructor(
    private prisma: PrismaService,
    private project: ProjectService,
  ) {
    this.log.info('initialized');
  }

  /**
   * Get a time budget by its id.
   */
  async findById(id: number): Promise<TimeBudget | null> {
    this.log.debug('Find by id', id);
    return this.prisma.timeBudget.findUnique({ where: { id } });
  }

  /**
   * Search project.
   */
  async search(params: { projectId?: number; cursor?: number; limit?: number }): Promise<CursorResult<TimeBudget>> {
    // TODO: add sort and search params ?
    this.log.debug('Search', params);

    // build the query
    const whereQuery = {
      projectId: params.projectId,
    };

    // data result
    const result = await this.prisma.timeBudget.findMany({
      take: params.limit || 10,
      where: whereQuery,
      cursor: params.cursor ? { id: params.cursor } : undefined,
      orderBy: { id: 'asc' },
    });

    // total count result
    const count = await this.prisma.timeBudget.count({ where: whereQuery });

    return {
      result,
      total: count,
      cursor: count !== result.length ? last(result)?.id || null : null,
    };
  }

  /**
   * Create a time budget for a project.
   */
  async create(projectId: number, data: TimeBudgetCreation): Promise<TimeBudget> {
    this.log.debug('Create', { projectId, data });

    // TODO: can't create a budget on an archived project

    // Find the related project
    const project = await this.project.findById(projectId);
    if (!project) throw new OctaveBusinessError('PROJECT_NOT_FOUND');
    if (project.archived) throw new OctaveBusinessError('PROJECT_ARCHIVED');

    // Create the budget
    const timeBudget = await this.prisma.timeBudget.create({
      data: {
        ...data,
        projectId: project.id,
      },
    });

    // Return it
    return timeBudget;
  }

  /**
   * Update a time budget.
   */
  async update(id: number, data: Partial<TimeBudgetCreation>): Promise<TimeBudget | null> {
    this.log.debug('Update', { id, data });

    const timeBudget = await this.findById(id);
    // If  not found, return null
    if (!timeBudget) return null;

    // Check if project is archived
    const project = await this.project.findById(timeBudget.id);
    if (project?.archived) throw new OctaveBusinessError('PROJECT_ARCHIVED');

    // Do the update
    const timeBudgetUpdated = await this.prisma.timeBudget.update({
      where: { id },
      data,
    });

    return timeBudgetUpdated;
  }

  /**
   * Delete a time budget for a project.
   */
  async delete(id: number): Promise<TimeBudget | null> {
    this.log.debug('Delete', { id });

    // If  not found, return null
    const timeBudget = await this.findById(id);
    if (!timeBudget) return null;

    // Check if project is archived
    const project = await this.project.findById(timeBudget.id);
    if (project && project?.archived) throw new OctaveBusinessError('PROJECT_ARCHIVED');

    // Deletion
    const timeBudgetDeleted = await this.prisma.timeBudget.delete({ where: { id } });
    return timeBudgetDeleted;
  }
}
