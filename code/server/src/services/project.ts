import { Injectable } from '@nestjs/common';
import { Project } from '@prisma/client';
import { last } from 'lodash';

import { OctaveBusinessError } from '../models/error';
import { CursorResult } from '../models';
import { getLogger } from './logger';
import { PrismaService } from './prisma';

@Injectable()
export class ProjectService {
  /**
   * Logger.
   */
  private log = getLogger('ProjectService');

  /**
   * Default constructor.
   */
  constructor(private prisma: PrismaService) {
    this.log.info('initialized');
  }

  private async isEmpty(id: number): Promise<boolean> {
    const countActivities = await this.prisma.activity.count({ where: { projectId: id } });
    return countActivities === 0;
  }

  /**
   * Search project.
   */
  async search(params: {
    name?: string;
    tags?: string[];
    cursor?: number;
    limit?: number;
  }): Promise<CursorResult<Project>> {
    this.log.debug('Search', params);
    // build the query
    const whereQuery = {
      name: { contains: params.name },
      tags: { hasEvery: params.tags },
    };

    // data result
    const result = await this.prisma.project.findMany({
      take: params.limit || 10,
      where: whereQuery,
      cursor: params.cursor ? { id: params.cursor } : undefined,
      orderBy: { id: 'asc' },
    });

    // total count result
    const count = await this.prisma.project.count({ where: whereQuery });

    return {
      result,
      total: count,
      cursor: count !== result.length ? last(result)?.id || null : null,
    };
  }

  /**
   * Get a user by its id.
   */
  async findById(id: number): Promise<Project | null> {
    this.log.debug('Find by id', id);
    return this.prisma.project.findUnique({ where: { id } });
  }

  /**
   * Create a project.
   */
  async create(data: Omit<Project, 'id' | 'createdAt' | 'updatedAt' | 'archived'>): Promise<Project> {
    this.log.debug('Create', data);
    return this.prisma.project.create({ data });
  }

  /**
   * Update a project.
   */
  async update(id: number, data: Partial<Project>): Promise<Project | null> {
    this.log.debug('Update', { id, data });

    // Find the project by its id
    const project = await this.findById(id);

    // If project is not found, return null
    if (!project) return null;

    // Save the project
    const updatedProject = await this.prisma.project.update({
      where: { id },
      data,
    });

    // Return the project
    return updatedProject;
  }

  /**
   * Delete a project
   */
  async delete(id: number, force = false): Promise<Project | null> {
    this.log.debug('Delete', { id });

    // Find the project by its id
    const project = await this.findById(id);
    if (!project) return null;

    // Check if project can be deleted or not
    if (!force) {
      const isEmpty = await this.isEmpty(id);
      if (!isEmpty) throw new OctaveBusinessError('PROJECT_NOT_EMPTY');
    }

    // Save the project
    const deletedProject = await this.prisma.project.delete({
      where: { id },
    });

    // Return the project
    return deletedProject;
  }
}
