import { Injectable } from '@nestjs/common';
import { User } from '@prisma/client';
import { last, omit } from 'lodash';
import bcrypt from 'bcrypt';

import { config } from '../config';
import { TokenData, CursorResult } from '../models';
import { OctaveBusinessError } from '../models/error';
import { checkEmail } from '../utils/email';
import { checkPassword } from '../utils/password';
import { getLogger } from './logger';
import { MailerService } from './mailer';
import { PrismaService } from './prisma';
import { TokenService } from './token';

@Injectable()
export class UserService {
  /**
   * Logger.
   */
  private log = getLogger('UserService');

  /**
   * Default constructor.
   */
  constructor(
    private prisma: PrismaService,
    private mailer: MailerService,
    private token: TokenService,
  ) {
    this.initDb();
    this.log.info('initialized');
  }

  /**
   * Send the email confirmation email.
   */
  private async doEmailConfirmation(user: User): Promise<void> {
    // Generate a token
    const token = await this.token.generate<TokenData>({ type: 'email_confirmation', id: user.id });
    // Send the email
    await this.mailer.userConfirmation(user, token);
  }

  /**
   * Send the forgot password email.
   */
  private async doForgotPassword(user: User): Promise<void> {
    // Generate a token
    const token = await this.token.generate<TokenData>({ type: 'forgot_password', id: user.id });
    // Send the email
    await this.mailer.userForgotPassword(user, token);
  }

  /**
   * Init the DB with the configured admin account if there is no account in db.
   */
  async initDb(): Promise<void> {
    try {
      const userCount = await this.prisma.user.count();
      if (!userCount) {
        const admin = await this.create({
          name: 'Admin',
          email: config.initAdmin.email,
          password: config.initAdmin.password,
          avatar: null,
        });
        this.confirmed(admin.id);
        this.log.info('DB initialized');
      }
    } catch (e) {
      this.log.error('Failded to initialize the DB', e);
    }
  }

  /**
   * Search users by name or email.
   */
  async search(params: {
    name?: string;
    email?: string;
    cursor?: number;
    limit?: number;
  }): Promise<CursorResult<User>> {
    // build the query
    const whereQuery = {
      name: { contains: params.name },
      email: { contains: params.email },
    };

    // data result
    const result = await this.prisma.user.findMany({
      take: params.limit || 10,
      where: whereQuery,
      cursor: params.cursor ? { id: params.cursor } : undefined,
      orderBy: { id: 'asc' },
    });

    // total count result
    const count = await this.prisma.user.count({ where: whereQuery });

    return {
      result,
      total: count,
      cursor: count !== result.length ? last(result)?.id || null : null,
    };
  }

  /**
   * Get a user by its id.
   */
  async findById(id: number): Promise<User | null> {
    return this.prisma.user.findUnique({ where: { id } });
  }

  /**
   * Get a user by its email.
   */
  async findByEmail(email: string): Promise<User | null> {
    return this.prisma.user.findUnique({ where: { email } });
  }

  /**
   * Create a user.
   */
  async create(data: Omit<User, 'id' | 'createdAt' | 'updatedAt' | 'confirmed'>): Promise<User> {
    // Check email validity
    if (!checkEmail(data.email)) throw new OctaveBusinessError('USER_BAD_EMAIL');

    // Check and encrypt password
    const password = await UserService.checkAndEncryptPassword(data.password);

    // Save the user
    const user = await this.prisma.user.create({ data: { ...data, password } });

    // Send account confirmation
    this.doEmailConfirmation(user);

    // Return the user
    return user;
  }

  /**
   * Update a user (except its password)
   */
  async update(id: number, data: Partial<User>): Promise<User | null> {
    // Find the user by its id
    const user = await this.findById(id);

    // If user is not found, return null
    if (!user) return null;

    // Checking the data to update (and avoid data managed by the app)
    const newData: Partial<User> = { ...omit(data, ['id', 'password', 'createdAt', 'updatedAt', 'confirmed']) };

    // Do the email has changed ?
    let shouldSendEmailConfirmation = false;
    if (data.email && data.email !== user.email) {
      // check email validity
      if (!checkEmail(data.email)) throw new OctaveBusinessError('USER_BAD_EMAIL');
      newData.confirmed = false;
      shouldSendEmailConfirmation = true;
    }

    // Change password ?
    if (data.password) {
      const encrypted = await UserService.checkAndEncryptPassword(data.password);
      newData.password = encrypted;
    }

    // Save the user
    const updatedUser = await this.prisma.user.update({
      where: { id },
      data: newData,
    });

    // Send the email confirmation if needed
    if (shouldSendEmailConfirmation) this.doEmailConfirmation(updatedUser);

    // Return the user
    return updatedUser;
  }

  /**
   * Confirm a user account.
   */
  async confirmed(id: number): Promise<User | null> {
    // Save the user
    const updatedUser = await this.prisma.user.update({
      where: { id },
      data: { confirmed: true },
    });

    // Return the user
    return updatedUser;
  }

  /**
   * Forgot password.
   */
  async forgotPassword(id: number): Promise<User | null> {
    // find the user by its id
    const user = await this.findById(id);
    // if user is not found, return null
    if (!user) return null;

    // Do the forgot password
    await this.doForgotPassword(user);

    // Return the user
    return user;
  }

  /**
   * Compare a clear password with its encrypted version.
   *
   * @param password The clear password to check
   * @param hash The encrypted password
   * @returns <code>true</code> if passwords are the same, <code>false</code> otherwise
   */
  static checkPassword(password: string, hash: string): Promise<boolean> {
    return new Promise((resolve) => {
      bcrypt.compare(password, hash, (_error, same: boolean) => {
        if (same) return resolve(true);
        return resolve(false);
      });
    });
  }

  /**
   * Check the password validity and returns its encrypted version
   */
  static checkAndEncryptPassword(password: string): Promise<string> {
    // Check the password
    const checks = checkPassword(password);
    if (!checks.isValid)
      throw new OctaveBusinessError('WEAK_PASSWORD', checks.missingRules.map((r) => r.name).join(', '));

    // Returns its encrypted version (bcrypt with salt)
    return bcrypt.hash(password, 10);
  }
}
