import { Injectable } from '@nestjs/common';
import Email from 'email-templates';
import * as path from 'path';
import sanitizeHtml from 'sanitize-html';

import { User } from '@prisma/client';
import { config } from '../../config';
import { getLogger, Logger } from '../logger';

@Injectable()
export class MailerService {
  /**
   * Logger.
   */
  private log: Logger = getLogger('Mailer');

  /**
   * Email template mailer.
   */
  private mailer: Email;

  /**
   * Constructor.
   */
  constructor() {
    this.log.info('initialized', config.mailer);
    this.mailer = new Email({
      views: {
        root: path.join(__dirname, 'template'),
        options: {
          extension: 'ejs',
        },
      },
      message: { from: config.mailer.noReply },
      send: true,
      transport: config.mailer.transport,
    });
  }

  /**
   * Generic method to send an email via a template.
   */
  private async send(
    email: string,
    template: string,
    params: { [key: string]: unknown },
    headers?: { [key: string]: string },
  ): Promise<void> {
    try {
      await this.mailer.send({
        template,
        message: {
          to: email,
          headers,
        },
        locals: { ...params, config, sanitizeHtml },
      });
    } catch (e) {
      this.log.error(`Failed to send email`, e);
    }
  }

  /**
   * Send forgot password email.
   */
  userForgotPassword(user: User, token: string): Promise<void> {
    return this.send(user.email, 'userForgotPassword', { user, token });
  }

  /**
   * Send account confirmation email.
   */
  userConfirmation(user: User, token: string): Promise<void> {
    return this.send(user.email, 'userConfirmation', { user, token });
  }
}
