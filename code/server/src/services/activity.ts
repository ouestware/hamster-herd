import { Injectable } from '@nestjs/common';
import { last } from 'lodash';

import { Activity, ActivityStatus } from '@prisma/client';
import { CursorResult } from '../models';
import { OctaveBusinessError } from '../models/error';
import { getLogger } from './logger';
import { PrismaService } from './prisma';
import { ProjectService } from './project';
import { UserService } from './user';

type ActivityCreation = Omit<Activity, 'id' | 'createdAt' | 'updatedAt'>;
type ActivityUpdate = Omit<Activity, 'id' | 'createdAt' | 'updatedAt' | 'userId' | 'projectId'>;

@Injectable()
export class ActivityService {
  /**
   * Logger.
   */
  private log = getLogger('ActivityService');

  /**
   * Default constructor.
   */
  constructor(
    private prisma: PrismaService,
    private project: ProjectService,
    private user: UserService,
  ) {
    this.log.info('initialized');
  }

  /**
   * Get an activity by its id.
   */
  async findById(id: number): Promise<Activity | null> {
    this.log.debug('Find by id', id);
    return this.prisma.activity.findUnique({ where: { id } });
  }

  /**
   * Search project.
   */
  async search(params: {
    projectId?: number;
    userId: number;
    status?: ActivityStatus;
    startedAt?: Date;
    endedAt?: Date;
    cursor?: number;
    limit?: number;
  }): Promise<CursorResult<Activity>> {
    // TODO: add sort and search params ?
    this.log.debug('Search', params);

    // build the query
    const whereQuery = {
      status: params.status,
      startedAt: { gte: params.startedAt },
      endedAt: { gte: params.endedAt },
      projectId: params.projectId,
      userId: params.userId,
    };

    // data result
    const result = await this.prisma.activity.findMany({
      take: params.limit || 10,
      where: whereQuery,
      cursor: params.cursor ? { id: params.cursor } : undefined,
      orderBy: { id: 'asc' },
    });

    // total count result
    const count = await this.prisma.activity.count({ where: whereQuery });

    return {
      result,
      total: count,
      cursor: count !== result.length ? last(result)?.id || null : null,
    };
  }

  /**
   * Create an activty for a project.
   */
  async create(data: ActivityCreation): Promise<Activity> {
    this.log.debug('Create', { data });

    // Find the related project
    const project = await this.project.findById(data.projectId);
    if (!project) throw new OctaveBusinessError('PROJECT_NOT_FOUND');
    if (project.archived) throw new OctaveBusinessError('PROJECT_ARCHIVED');

    // Find the related user
    const user = await this.user.findById(data.userId);
    if (!user) throw new OctaveBusinessError('USER_NOT_FOUND');

    // Creation
    const activity = await this.prisma.activity.create({
      data: {
        ...data,
        projectId: project.id,
        userId: user.id,
      },
    });

    // Return it
    return activity;
  }

  /**
   * Update an activity.
   */
  async update(id: number, data: Partial<ActivityUpdate>): Promise<Activity | null> {
    this.log.debug('Update', { id, data });
    // TODO: can't update an activity on an archived project
    // TODO : check status changed ?

    // Do the update
    const activity = await this.prisma.activity.update({
      where: { id },
      data,
    });

    // If  not found, return null
    if (!activity) return null;

    return activity;
  }

  /**
   * Delete a time budget for a project.
   */
  async delete(id: number): Promise<Activity | null> {
    this.log.debug('Delete', { id });
    // TODO: can't update an activity on an archived project

    const activity = await this.prisma.activity.delete({ where: { id } });

    // If  not found, return null
    if (!activity) return null;

    return activity;
  }
}
