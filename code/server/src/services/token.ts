import { Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import UIDGenerator from 'uid-generator';

import { Prisma } from '@prisma/client';
import { OctaveBusinessError } from '../models/error';
import { PrismaService } from './prisma';
import { getLogger } from './logger';

@Injectable()
export class TokenService {
  /**
   * Logger.
   */
  private log = getLogger('TokenService');

  /**
   * A simple id generator
   */
  private uidgen = new UIDGenerator(256);

  /**
   * Default constructor.
   */
  constructor(private prisma: PrismaService) {
    this.log.info('initialized');
  }

  /**
   *  Compute the min valid date for a token.
   */
  private minValidDate(): Date {
    const date = new Date();
    date.setHours(date.getHours() - 4);
    return date;
  }

  /**
   * Generate a token and save it in the db with the provided value.
   */
  async generate<T extends Prisma.JsonObject>(data: T): Promise<string> {
    const token = await this.uidgen.generate();
    this.log.debug('generate token', { token, data });
    await this.prisma.token.create({
      data: { token, data },
    });
    return token;
  }

  /**
   * Check if the token is valid and returns the associated value, null otherwise.
   * Reading the token, delete it.
   */
  async check<T extends Prisma.JsonObject>(token: string): Promise<T | null> {
    this.log.debug('check token', token);

    // Read and delete the token
    const tokenRecord = await this.prisma.token.delete({ where: { token } });

    // If no token is found, returns null
    if (!tokenRecord) return null;

    // Check the token validity
    if (tokenRecord.createdAt < this.minValidDate()) throw new OctaveBusinessError('TOKEN_EXPIRED');

    // Returns the token related data
    return tokenRecord.data as T;
  }

  /**
   * Remove all token entries in the DB that are older than 4h.
   */
  @Cron('* * 4 * * *')
  async cleanDb(): Promise<void> {
    await this.prisma.token.deleteMany({ where: { createdAt: { lt: this.minValidDate() } } });
  }
}
