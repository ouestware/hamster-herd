import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

import { OctaveBusinessError } from '../models/error';
import { toUserDto } from '../models/user';
import { getLogger } from './logger';
import { UserService } from './user';

@Injectable()
export class AuthService {
  /**
   * Logger.
   */
  private log = getLogger('AuthService');

  /**
   * Default constructor.
   */
  constructor(
    private user: UserService,
    private jwtService: JwtService,
  ) {
    this.log.info('initialized');
  }

  async login(email: string, password: string): Promise<string | null> {
    const user = await this.user.findByEmail(email);
    if (!user) return null;

    const check = await UserService.checkPassword(password, user.password);
    if (!check) throw new OctaveBusinessError('LOGIN_FAILED');

    // we adding sub to respect the jwt standard
    return this.jwtService.signAsync({ ...toUserDto(user), sub: user.id });
  }
}
