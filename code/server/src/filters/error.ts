import { ExceptionFilter, Catch, ArgumentsHost, HttpException } from '@nestjs/common';
import { Request, Response } from 'express';
import { getLogger } from '../services/logger';
import { OctaveBusinessError } from '../models/error';

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
  /**
   * Logger.
   */
  private log = getLogger('ErrorFilter');

  constructor() {
    this.log.info('initialized');
  }

  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();

    // TODO: check exception and cast it to http exception
    if (exception instanceof OctaveBusinessError) {
      response.status(400).json({
        statusCode: 400,
        timestamp: new Date().toISOString(),
        path: request.url,
      });
    } else {
      const status = exception.getStatus();
      response.status(status).json({
        statusCode: status,
        timestamp: new Date().toISOString(),
        path: request.url,
      });
    }
  }
}
