import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

import { AppModule } from './app.module';
import { config } from './config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.setGlobalPrefix('api/v1.0');
  app.enableCors();

  // swagger
  const swaggerDocument = new DocumentBuilder()
    .setTitle('Octave')
    .setDescription('Octave API')
    .setVersion('1.0')
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, swaggerDocument);
  SwaggerModule.setup('api/docs', app, document);

  // run the app
  await app.listen(config.port, config.host);
  console.log(`Server is available at http://${config.host}:${config.port}`);
}

bootstrap();
