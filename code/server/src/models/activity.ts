import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

import { Activity, ActivityStatus } from '@prisma/client';
import { CursorResult } from '.';

export class ActivityDto {
  @ApiProperty()
  id: number;

  @ApiProperty()
  createdAt: Date;

  @ApiProperty()
  updatedAt: Date;

  @ApiProperty()
  startedAt: Date;

  @ApiProperty()
  endedAt: Date;

  @ApiProperty({ enum: ActivityStatus })
  status: ActivityStatus;

  @ApiPropertyOptional()
  description?: string | null;

  @ApiPropertyOptional()
  tags?: string[];

  @ApiProperty()
  projectId: number;

  @ApiProperty()
  userId: number;
}

export class ActivityCreationDto {
  @ApiProperty()
  projectId: number;

  @ApiProperty({ enum: ActivityStatus })
  status: ActivityStatus;

  @ApiProperty()
  startedAt: Date;

  @ApiProperty()
  endedAt: Date;

  @ApiPropertyOptional()
  description?: string | null;

  @ApiPropertyOptional()
  tags?: string[];
}

export class ActivityUpdateDto {
  @ApiProperty()
  startedAt: Date;

  @ApiProperty()
  endedAt: Date;

  @ApiPropertyOptional({ enum: ActivityStatus })
  status?: ActivityStatus;

  @ApiPropertyOptional()
  description?: string | null;

  @ApiPropertyOptional()
  tags?: string[];
}

export class ActivityCursorResult implements CursorResult<ActivityDto> {
  @ApiProperty({ isArray: true, type: ActivityDto })
  result: Array<ActivityDto>;

  @ApiProperty({ nullable: true })
  cursor: number | null;

  @ApiProperty()
  total: number;
}

export function toActivityDto(activity: Activity): ActivityDto {
  return activity;
}
