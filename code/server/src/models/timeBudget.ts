import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

import { TimeBudget } from '@prisma/client';
import { CursorResult } from '.';

export class TimeBudgetDto {
  @ApiProperty()
  id: number;

  @ApiProperty()
  createdAt: Date;

  @ApiProperty()
  updatedAt: Date;

  @ApiProperty()
  duration: number;

  @ApiPropertyOptional()
  description?: string | null;
}

export class TimeBudgetCreationDto {
  @ApiProperty()
  duration: number;

  @ApiPropertyOptional()
  description?: string | null;
}

export class TimeBudgetUpdateDto {
  @ApiPropertyOptional()
  duration?: number;

  @ApiPropertyOptional()
  description?: string | null;
}

export class TimeBudgetCursorResult implements CursorResult<TimeBudgetDto> {
  @ApiProperty({ isArray: true, type: TimeBudgetDto })
  result: Array<TimeBudgetDto>;

  @ApiProperty({ nullable: true })
  cursor: number | null;

  @ApiProperty()
  total: number;
}

export function toTimeBudgetDto(timeBudget: TimeBudget): TimeBudgetDto {
  return timeBudget;
}
