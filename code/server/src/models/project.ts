import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { CursorResult } from '.';
import { Project } from '@prisma/client';

export class ProjectDto {
  @ApiProperty()
  id: number;

  @ApiProperty()
  createdAt: Date;

  @ApiProperty()
  updatedAt: Date;

  @ApiProperty()
  archived: boolean;

  @ApiProperty()
  name: string;

  @ApiPropertyOptional()
  description?: string | null;

  @ApiPropertyOptional()
  tags?: string[];
}

export class ProjectCreationDto {
  @ApiProperty()
  name: string;

  @ApiPropertyOptional()
  description?: string | null;

  @ApiPropertyOptional()
  tags?: string[];
}

export class ProjectUpdateDto {
  @ApiPropertyOptional()
  name: string;

  @ApiPropertyOptional()
  description?: string | null;

  @ApiPropertyOptional()
  tags?: string[];

  @ApiPropertyOptional()
  archived?: boolean;
}

export class ProjectCursorResult implements CursorResult<ProjectDto> {
  @ApiProperty({ isArray: true, type: ProjectDto })
  result: Array<ProjectDto>;

  @ApiProperty({ nullable: true })
  cursor: number | null;

  @ApiProperty()
  total: number;
}

export function toProjectDto(project: Project): ProjectDto {
  return project;
}
