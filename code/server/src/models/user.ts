import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { omit } from 'lodash';
import { User } from '@prisma/client';
import { CursorResult } from '.';

export class UserDto {
  @ApiProperty()
  id: number;

  @ApiProperty()
  email: string;

  @ApiProperty()
  name: string;

  @ApiProperty()
  confirmed: boolean;

  @ApiProperty()
  createdAt: Date;

  @ApiProperty()
  updatedAt: Date;

  @ApiPropertyOptional({ nullable: true })
  avatar: string | null;
}

export class UserCreationDto {
  @ApiProperty()
  email: string;

  @ApiProperty()
  name: string;

  @ApiProperty()
  password: string;

  @ApiPropertyOptional({ nullable: true })
  avatar: string | null;
}

export class UserUpdateDto {
  @ApiPropertyOptional()
  email?: string;

  @ApiPropertyOptional()
  name?: string;

  @ApiPropertyOptional()
  password?: string;

  @ApiPropertyOptional({ nullable: true })
  avatar?: string | null;
}

export class UserLoginDto {
  @ApiProperty()
  email: string;

  @ApiProperty()
  password: string;
}

export class UserCursorResult implements CursorResult<UserDto> {
  @ApiProperty({ isArray: true, type: UserDto })
  result: Array<UserDto>;

  @ApiProperty({ nullable: true })
  cursor: number | null;

  @ApiProperty()
  total: number;
}

export function toUserDto(user: User): UserDto {
  return omit(user, ['password']);
}
