import { Prisma } from '@prisma/client';

export class CursorResult<T> {
  result: Array<T>;
  cursor: number | null;
  total: number;
}

export interface TokenData extends Prisma.JsonObject {
  type: string;
  id: number;
}
