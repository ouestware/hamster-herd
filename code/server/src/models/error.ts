export class OctaceError extends Error {
  private code: string;

  constructor(code: string, message?: string) {
    super(message);
    this.name = 'OctaveError';
    this.code = code;
  }

  toString() {
    return `${this.code} - ${this.message}`;
  }
}

export class OctaveTechnicalError extends OctaceError {
  constructor(code: string, message?: string) {
    super(code, message);
    this.name = 'OctaveTechnicalError';
  }
}

export class OctaveBusinessError extends OctaceError {
  constructor(code: string, message?: string) {
    super(code, message);
    this.name = 'OctaveBusinessError';
  }
}
