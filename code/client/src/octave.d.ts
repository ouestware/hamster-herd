export type ModelNameType = "User" | "Project" | "TimeSpan" | "TimeBudget";

export interface UserType {
  id: string;
  name: string;
  avatarUrl?: string;
}

export interface ProjectType {
  id: string;
  name: string;
  description?: string;
  tags?: string[];
}

export interface TimeSpanType {
  id: string;
  user: UserType; // should this be an array ?
  project: ProjectType;
  startTime: string; // TODO: confirm date format strategy
  status: "planned" | "done";
  endTime: string; // TODO: confirm date format strategy
  extraDuration?: number; // TODO: discuss duration format
  description?: string;
  tags?: string[];
}

export interface TimeBudgetType {
  id: string;
  date: string; // TODO: confirm date format strategy
  duration: number; // TODO: discuss duration format /!\ can be negative
  project: ProjectType;
  description?: string;
}

export type AnyItemType = TimeBudgetType | TimeSpanType | ProjectType | UserType;
