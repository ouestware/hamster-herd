import { FC } from "react";
import { useParams } from "react-router-dom";
import { useGetItem } from "../hooks/useGetData";
import { ProjectType } from "../octave";

export const ProjectPage: FC = () => {
  const { id } = useParams();
  const project = useGetItem<ProjectType>("Project", id);

  return <div>{project ? project.name : id}</div>;
};
