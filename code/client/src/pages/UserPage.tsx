import { FC } from "react";
import { useParams } from "react-router-dom";
import { useGetItem } from "../hooks/useGetData";
import { UserType } from "../octave";

export const UserPage: FC = () => {
  const { id } = useParams();
  const user = useGetItem<UserType>("User", id);
  return <div>{user ? user.name : id}</div>;
};
