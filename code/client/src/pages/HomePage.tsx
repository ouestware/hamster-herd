import { FC } from "react";
import { PageLayout } from "../components/PageLayout";
import { TimeSpanForm } from "../components/TimeSpan/TimeSpanForm";
import { TimeSpanList } from "../components/TimeSpan/TimeSpanList";

export const HomePage: FC = () => {
  return (
    <PageLayout>
      <h1>Home Page</h1>

      <TimeSpanForm />
      <TimeSpanList />
    </PageLayout>
  );
};
