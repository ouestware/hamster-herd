import { FC, useEffect, useState } from "react";
import { useLazySearchItem } from "../../hooks/useGetData";
import { TimeSpanType } from "../../octave";

export const TimeSpanList: FC = () => {
  const { searchItem: searchTimeSpan } = useLazySearchItem<TimeSpanType>("TimeSpan", []);
  const [timeSpans, setTimeSpans] = useState<TimeSpanType[]>([]);

  //TODO: use a direct query hook
  useEffect(() => {
    searchTimeSpan("").then((ts) => setTimeSpans(ts));
  }, [searchTimeSpan]);
  console.log(timeSpans);
  return (
    <div>
      {timeSpans.map((ts) => (
        <div key={ts.id}>
          {" "}
          @{ts.user.name} /{ts.project.name} {ts.startTime}-{ts.endTime} {ts.description}
        </div>
      ))}
    </div>
  );
};
