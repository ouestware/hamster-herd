import cx from "classnames";
import { FC, useEffect, useState } from "react";
import AsyncSelect from "react-select/async";

import { useLazySearchItem, useUpsertItem } from "../../hooks/useGetData";
import { ProjectType, TimeSpanType, UserType } from "../../octave";

export const TimeSpanForm: FC<{ timeSpan?: TimeSpanType }> = ({ timeSpan }) => {
  const [validationState, setValidationState] = useState<boolean>(false);
  const [editedTimeSpan, setEditedTimeSpan] = useState<Partial<TimeSpanType> | undefined>(timeSpan);

  const { searchItem: searchUser } = useLazySearchItem<UserType>("User", ["name"]);
  const { searchItem: searchProject } = useLazySearchItem<ProjectType>("Project", ["name"]);
  const { upsertItem: upsertTimespan } = useUpsertItem<TimeSpanType>("TimeSpan");

  useEffect(() => {
    const valid = !!editedTimeSpan && !!editedTimeSpan.user && !!editedTimeSpan.project && !!editedTimeSpan.startTime;
    setValidationState(valid);
  }, [editedTimeSpan]);

  return (
    <form
      onSubmit={(e) => {
        e.preventDefault();
        if (editedTimeSpan && validationState) upsertTimespan(editedTimeSpan);
      }}
    >
      <div className="row g-3 mb-3">
        <div className="col-4">
          <label htmlFor="user" className="form-label">
            Personne
          </label>
          <div className="input-group has-validation">
            <span className="input-group-text">@</span>
            <AsyncSelect
              id="user"
              required
              isClearable
              className={cx("flex-grow-1", editedTimeSpan && editedTimeSpan.user === undefined && "is-invalid")}
              aria-describedby="userHelp"
              loadOptions={searchUser}
              getOptionLabel={(u) => u.name}
              defaultOptions={true}
              onChange={(user) => {
                setEditedTimeSpan({ ...editedTimeSpan, user: user || undefined });
              }}
            />
            <div id="validationServerUsernameFeedback" className="invalid-feedback">
              Personne obligatoire
            </div>
          </div>

          <div id="userHelp" className="form-text">
            Est ce qu'on ne devrait pas autoriser plusieurs personnes ?
          </div>
        </div>
        <div className="col-4">
          <label htmlFor="project" className="form-label">
            Projet
          </label>
          <div className="input-group has-validation">
            <span className="input-group-text">/</span>
            <AsyncSelect
              id="project"
              required
              isClearable
              className={cx("flex-grow-1", editedTimeSpan && editedTimeSpan.project === undefined && "is-invalid")}
              aria-describedby="projectHelp"
              loadOptions={searchProject}
              getOptionLabel={(p) => p.name}
              defaultOptions={true}
              onChange={(project) => {
                setEditedTimeSpan({ ...editedTimeSpan, project: project || undefined });
              }}
            />
            <div id="validationServerUsernameFeedback" className="invalid-feedback">
              Projet obligatoire
            </div>
          </div>
          <div id="projectHelp" className="form-text">
            Projet ou Activité ?
          </div>
        </div>
        <div className="col-4">
          <label htmlFor="project" className="form-label">
            Tags
          </label>
          <AsyncSelect isDisabled id="tags" isMulti />

          <div id="projectHelp" className="form-text">
            TODO: timespan tags
          </div>
        </div>
        <div className="col-4">
          <label htmlFor="start" className="form-label">
            Début
          </label>
          <input
            type="datetime-local"
            className="form-control"
            id="start"
            required
            aria-describedby="startHelp"
            // replace format hack by proper method
            value={editedTimeSpan?.startTime || ""}
            onChange={(e) => {
              setEditedTimeSpan({
                ...editedTimeSpan,
                startTime: e.target.value || undefined,
              });
            }}
          />
          <div id="startHelp" className="form-text">
            Est ce qu'on ne devrait pas autoriser plusieurs personnes ?
          </div>
        </div>
        <div className="col-4">
          <label htmlFor="end" className="form-label">
            Fin
          </label>
          <input
            type="datetime-local"
            className="form-control"
            id="end"
            // replace format hack by proper method .toLocaleString("sv").replace(" ", "T") || ""
            value={editedTimeSpan?.endTime || ""}
            aria-describedby="endHelp"
            onChange={(e) => {
              setEditedTimeSpan({
                ...editedTimeSpan,
                endTime: e.target.value || undefined,
              });
            }}
          />
          <div id="endHelp" className="form-text">
            Est ce qu'on ne devrait pas autoriser plusieurs personnes ?
          </div>
        </div>
        <div className="col-4">
          <label htmlFor="extra" className="form-label">
            Extra
          </label>
          <input
            type="number"
            id="extra"
            value={editedTimeSpan?.extraDuration}
            className="form-control"
            step={1}
            aria-describedby="extraHelp"
            onChange={(e) => {
              setEditedTimeSpan({ ...editedTimeSpan, extraDuration: e.target.value ? +e.target.value : undefined });
            }}
          />
          <div id="extraHelp" className="form-text">
            (en heure) Temps supplémentaire pour couvrir des frais annexes
          </div>
        </div>
        <div className="col-3">
          <label htmlFor="planlog" className="form-label">
            log/plan (TODO)
          </label>
          <select
            id="planlog"
            className="form-control"
            onChange={(e) => {
              setEditedTimeSpan({
                ...editedTimeSpan,
                status: e.target.value ? (e.target.value as "planned" | "done") : undefined,
              });
            }}
            value={editedTimeSpan?.status || "done"}
          >
            <option value="done">fait</option>
            <option value="planned">à faire</option>
          </select>
        </div>
        <div className="col-12">
          <label htmlFor="description" className="form-label">
            Description
          </label>
          <textarea
            id="description"
            className="form-control"
            value={editedTimeSpan?.description}
            onChange={(e) => {
              setEditedTimeSpan({ ...editedTimeSpan, description: e.target.value ? e.target.value : undefined });
            }}
          />
        </div>
        <button className="btn btn-primary" type="submit" disabled={!editedTimeSpan || !validationState}>
          {editedTimeSpan?.id ? "sauver" : "créer"}
        </button>
      </div>
    </form>
  );
};
