import { FC } from "react";
import { useInitData } from "../hooks/useGetData";

export const InitData: FC = () => {
  useInitData();
  return null;
};
