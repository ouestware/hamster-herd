import cx from "classnames";
import { FC, PropsWithChildren } from "react";

export const PageLayout: FC<{ fluid?: boolean } & PropsWithChildren> = ({ fluid, children }) => {
  return <div className={cx(fluid ? "container-fluid" : "container")}>{children}</div>;
};
