import { RouteObject } from "react-router-dom";
import { HomePage } from "./pages/HomePage";
import { ProjectPage } from "./pages/Project";
import { UserPage } from "./pages/UserPage";

const routes: RouteObject[] = [
  {
    path: "/",
    element: <HomePage />,
  },
  { path: "project/:id", element: <ProjectPage /> },
  { path: "user/:id", element: <UserPage /> },
];

export default routes;
