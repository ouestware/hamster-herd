import { isString, values } from "lodash";
import { v4 as uuidv4 } from "uuid";

import { AnyItemType, ModelNameType, ProjectType, UserType } from "../octave";

const localStorageKey = (model: ModelNameType) => `octave-${model}`;

function getItem<T extends AnyItemType>(model: ModelNameType, id?: string) {
  if (!id) return null;
  const jsonItems = localStorage.getItem(localStorageKey(model));
  if (jsonItems) {
    return (JSON.parse(jsonItems)[id] as T) || null;
  }
  return null;
}

export function useGetItem<T extends AnyItemType>(model: ModelNameType, id?: string) {
  return getItem<T>(model, id);
}

export function useLazyGetItem<T extends AnyItemType>(model: ModelNameType) {
  return {
    getItem: async (id: string) => {
      return getItem<T>(model, id);
    },
  };
}

export function useUpsertItem<T extends AnyItemType>(model: ModelNameType) {
  return {
    upsertItem: async (item: Partial<T>) => {
      // for create case mimic id creation
      const id = item.id || uuidv4();

      const jsonItems = localStorage.getItem(localStorageKey(model));
      const items = jsonItems ? JSON.parse(jsonItems) : {};
      items[id] = { id, ...item };
      localStorage.setItem(localStorageKey(model), JSON.stringify(items));
    },
  };
}

export function useLazySearchItem<T extends AnyItemType>(model: ModelNameType, fields: (keyof T)[]) {
  return {
    searchItem: async (query: string) => {
      const jsonItems = localStorage.getItem(localStorageKey(model));
      const items = jsonItems ? (JSON.parse(jsonItems) as Record<string, T>) : {};
      // mock up search with includes
      return values(items).filter((i) =>
        query !== ""
          ? fields.some((f) => isString(i[f]) && (i[f] as string).toLowerCase().includes(query.toLowerCase()))
          : true,
      );
    },
  };
}

export const useInitData = async () => {
  const { upsertItem: upsertUser } = useUpsertItem<UserType>("User");
  const { getItem: getUser } = useLazyGetItem<UserType>("User");
  const { upsertItem: upsertProject } = useUpsertItem<ProjectType>("Project");
  const { getItem: getProject } = useLazyGetItem<ProjectType>("Project");

  console.log("upsert users: Paul, Benoît et Alexis");
  await Promise.all(
    ["Paul", "Alexis", "Benoît"].map(async (u) => {
      const id = u.toLowerCase();
      const user = await getUser(id);
      if (user === null) await upsertUser({ id, name: u });
    }),
  );

  console.log("upsert projects: MEL, OSRD, ChromoBase");

  await Promise.all(
    ["MEL", "OSRD", "ChromoBase"].map(async (p) => {
      const id = p.toLowerCase();
      const project = await getProject(id);
      if (project === null) await upsertProject({ id, name: p });
    }),
  );
  return;
};
