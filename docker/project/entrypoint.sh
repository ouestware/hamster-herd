#!/bin/bash
echo "(i) Npm version is $(npm -v)"
echo "(i) Node version is $(node -v)"

set -o allexport
source /.env
set +o allexport

cd /project/code

echo
echo " ~"
echo " ~ Install dependencies"
echo " ~"
echo
# Take ownership of the node_module
mkdir -p ./node_modules
sudo chown -R docker:docker ./node_modules
npm install

echo
echo " ~"
echo " ~ Start the web server"
echo " ~"
echo
sudo nginx -c /etc/nginx/nginx.conf

echo " ~"
echo " ~ Start project for dev"
echo " ~"
echo
npm start
